# Tanuki Racing Release Notes
## Version: 1.0.1, Nov 1, 2023

#### Updates:
- Spelling corrections in the Project Management course

#### Contributors:
- Chris Guitarte @cguitarte


## Version: 1.0.0, Oct 18, 2023

#### Updates:
- Init release of new application
- Choose your own adventure type, works for any major demo of GitLab features
- Init course release comes with AI, CICD, Security, & Project Management

#### Contributors:
- Logan Stucker @lfstucker
